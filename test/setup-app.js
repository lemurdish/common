import { jsdom } from 'jsdom';

global.document = jsdom('<!doctype html><html><body></body></html>', {
  url: 'https://app.lemurdish.com/'
});
global.window = document.defaultView;
global.navigator = global.window.navigator;
