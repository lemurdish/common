'use strict';

exports.__esModule = true;
exports.default = API;

require('babel-polyfill');

var _urijs = require('urijs');

var _urijs2 = _interopRequireDefault(_urijs);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function API() {
  var base = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'https://localhost:5000';

  var get = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(path, idToken) {
      var headers, response;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              headers = {
                'Authorization': 'Bearer ' + idToken
              };
              _context.next = 3;
              return axios.get(getPath(path), { headers: headers });

            case 3:
              response = _context.sent;
              return _context.abrupt('return', response.data);

            case 5:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    return function get(_x3, _x4) {
      return _ref.apply(this, arguments);
    };
  }();

  var post = function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(path, msg, idToken) {
      var headers, response;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              headers = {
                'Authorization': 'Bearer ' + idToken,
                'Content-Type': 'application/json'
              };
              _context2.next = 3;
              return axios.post(getPath(path), msg, { headers: headers });

            case 3:
              response = _context2.sent;
              return _context2.abrupt('return', response.data);

            case 5:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    return function post(_x5, _x6, _x7) {
      return _ref2.apply(this, arguments);
    };
  }();

  var fetchUser = function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(idToken) {
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt('return', get('/api/auth/get-user', idToken));

            case 1:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    return function fetchUser(_x8) {
      return _ref3.apply(this, arguments);
    };
  }();

  var upsertUser = function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(idToken) {
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              return _context4.abrupt('return', get('/api/auth/upsert-user', idToken));

            case 1:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, this);
    }));

    return function upsertUser(_x9) {
      return _ref4.apply(this, arguments);
    };
  }();

  var saveContact = function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(crmModel, idToken) {
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              return _context5.abrupt('return', post('/api/integrations/save-contact', crmModel, idToken));

            case 1:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, this);
    }));

    return function saveContact(_x10, _x11) {
      return _ref5.apply(this, arguments);
    };
  }();

  var fetchContact = function () {
    var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(crmModel, idToken) {
      var uri, headers, response;
      return regeneratorRuntime.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              uri = new _urijs2.default(base);

              uri.path('/api/integrations/fetch-contact');
              uri.setSearch(crmModel);
              headers = {
                'Authorization': 'Bearer ' + idToken
              };
              _context6.next = 6;
              return axios.get(uri.toString(), { headers: headers });

            case 6:
              response = _context6.sent;
              return _context6.abrupt('return', response.data);

            case 8:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, this);
    }));

    return function fetchContact(_x12, _x13) {
      return _ref6.apply(this, arguments);
    };
  }();

  var fetchSchema = function () {
    var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(idToken) {
      return regeneratorRuntime.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              return _context7.abrupt('return', get('/api/integrations/fetch-schema', idToken));

            case 1:
            case 'end':
              return _context7.stop();
          }
        }
      }, _callee7, this);
    }));

    return function fetchSchema(_x14) {
      return _ref7.apply(this, arguments);
    };
  }();

  var axios = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : axios;

  var getPath = function getPath(path) {
    var uri = new _urijs2.default(base);
    uri.path(path);
    return uri.toString();
  };

  return {
    get: get,
    post: post,
    fetchUser: fetchUser,
    integration: {
      saveContact: saveContact,
      fetchContact: fetchContact,
      fetchSchema: fetchSchema
    }
  };
}
module.exports = exports['default'];