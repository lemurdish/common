'use strict';

exports.__esModule = true;
var isRequired = exports.isRequired = function isRequired(v) {
  if (!v) {
    return 'This field is required';
  }
  return undefined;
};