'use strict';

exports.__esModule = true;
var prefix = 'INTERNAL_SCHEME';

var FIRST_NAME = exports.FIRST_NAME = prefix + '_FIRST_NAME';
var LAST_NAME = exports.LAST_NAME = prefix + '_LAST_NAME';
var EMAIL = exports.EMAIL = prefix + '_EMAIL';
var JOB_TITLE = exports.JOB_TITLE = prefix + '_JOB_TITLE';
var COMPANY = exports.COMPANY = prefix + '_COMPANY';
var LINKEDIN_PROFILE_URL = exports.LINKEDIN_PROFILE_URL = prefix + '_LINKEDIN_PROFILE_URL';

var ID = exports.ID = prefix + '_ID'; // ID for contacts retrieved from CRM
var VIEW_URL = exports.VIEW_URL = prefix + '_VIEW_URL'; // To view contact in particular CRM