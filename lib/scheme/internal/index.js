'use strict';

exports.__esModule = true;
exports.validators = exports.labels = exports.constants = undefined;

var _labels, _validators;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _constants = require('./constants');

var constants = _interopRequireWildcard(_constants);

var _validators2 = require('./validators');

var allValidators = _interopRequireWildcard(_validators2);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var labels = (_labels = {}, _labels[constants.FIRST_NAME] = 'First Name', _labels[constants.LAST_NAME] = 'Last Name', _labels[constants.EMAIL] = 'E-mail', _labels[constants.JOB_TITLE] = 'Job Title', _labels[constants.COMPANY] = 'Company', _labels[constants.LINKEDIN_PROFILE_URL] = 'LinkedIn Profile URL', _labels);

var validators = (_validators = {}, _validators[constants.FIRST_NAME] = allValidators.isRequired, _validators[constants.LAST_NAME] = allValidators.isRequired, _validators[constants.EMAIL] = _lodash2.default.noop, _validators[constants.JOB_TITLE] = _lodash2.default.noop, _validators[constants.COMPANY] = _lodash2.default.noop, _validators[constants.LINKEDIN_PROFILE_URL] = _lodash2.default.noop, _validators);

exports.constants = constants;
exports.labels = labels;
exports.validators = validators;