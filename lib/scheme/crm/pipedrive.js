'use strict';

exports.__esModule = true;
exports.fromInternal = exports.toInternal = exports.schema = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _fields = require('../fields');

var fields = _interopRequireWildcard(_fields);

var _constants = require('../internal/constants');

var internalSchemeConstants = _interopRequireWildcard(_constants);

var _utils = require('../utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var schema = exports.schema = [fields.text('name', function (model) {
  return utils.exportName(model.name);
}, function (internalModel) {
  var first = internalModel[internalSchemeConstants.FIRST_NAME];
  var last = internalModel[internalSchemeConstants.LAST_NAME];
  return {
    name: first + ' ' + last
  };
}), fields.text('email', function (model) {
  var _ref;

  return model.email ? (_ref = {}, _ref[internalSchemeConstants.EMAIL] = model.email, _ref) : {};
}, function (internalModel) {
  var email = internalModel[internalSchemeConstants.EMAIL];
  if (email) return { email: email };
  return {};
}), fields.text('url', function (model) {
  var _ref2;

  return model.id ? (_ref2 = {}, _ref2[internalSchemeConstants.VIEW_URL] = 'https://app.pipedrive.com/person/' + model.id, _ref2) : {};
}, function (internalModel) {
  return {};
}), fields.text('id', function (model) {
  var _ref3;

  return model.id ? (_ref3 = {}, _ref3[internalSchemeConstants.ID] = model.id, _ref3) : {};
}, function (internalModel) {
  var id = internalModel[internalSchemeConstants.ID];
  if (id) return { id: id };
  return null;
})];

var toInternal = exports.toInternal = function toInternal(crmModel) {
  return schema.reduce(function (acc, field) {
    return _extends({}, acc, field.exporter(crmModel));
  }, {});
};

var fromInternal = exports.fromInternal = function fromInternal(internalModel) {
  return schema.reduce(function (acc, field) {
    return _extends({}, acc, field.importer(internalModel));
  }, {});
};