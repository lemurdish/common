'use strict';

exports.__esModule = true;
exports.toInternal = exports.fromInternal = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _constants = require('../internal/constants');

var internalSchemeConstants = _interopRequireWildcard(_constants);

var _utils = require('../utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapping = {
  firstname: internalSchemeConstants.FIRST_NAME,
  lastname: internalSchemeConstants.LAST_NAME,
  email: internalSchemeConstants.EMAIL,
  jobtitle: internalSchemeConstants.JOB_TITLE,
  company: internalSchemeConstants.COMPANY,
  linkedinbio: internalSchemeConstants.LINKEDIN_PROFILE_URL
};

var fromInternal = exports.fromInternal = function fromInternal(internalModel) {
  var result = {};
  var properties = [];
  var profileUrl = internalModel[internalSchemeConstants.VIEW_URL];
  if (profileUrl) result['profile-url'] = profileUrl;
  _lodash2.default.toPairs(mapping).forEach(function (_ref) {
    var hubspotProperty = _ref[0],
        internalProperty = _ref[1];

    if (internalModel[internalProperty]) {
      properties.push({
        property: hubspotProperty,
        value: internalModel[internalProperty]
      });
    }
  });
  return _extends({
    properties: properties
  }, result);
};

var toInternal = exports.toInternal = function toInternal(hubspotModel) {
  var result = {};
  var profileUrl = hubspotModel['profile-url'];
  if (profileUrl) result[internalSchemeConstants.VIEW_URL] = profileUrl;
  if (Array.isArray(hubspotModel.properties)) {
    hubspotModel.properties.forEach(function (_ref2) {
      var property = _ref2.property,
          value = _ref2.value;

      if (mapping[property]) {
        result[mapping[property]] = value;
      }
    });
  } else {
    _lodash2.default.keys(mapping).forEach(function (property) {
      var info = hubspotModel.properties[property];
      if (info && info.value) result[mapping[property]] = info.value;
    });
  }
  return result;
};