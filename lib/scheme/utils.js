'use strict';

exports.__esModule = true;
exports.textTrimmer = exports.exportName = undefined;

var _parseFullName = require('parse-full-name');

var _constants = require('./internal/constants');

var internalSchemeConstants = _interopRequireWildcard(_constants);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var exportName = exports.exportName = function exportName() {
  var _ref;

  var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  var parsedName = (0, _parseFullName.parseFullName)(name);
  return _ref = {}, _ref[internalSchemeConstants.FIRST_NAME] = parsedName.first, _ref[internalSchemeConstants.LAST_NAME] = parsedName.last, _ref;
};
var textTrimmer = exports.textTrimmer = function textTrimmer(text) {
  return text.trim();
};