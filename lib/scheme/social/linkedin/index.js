'use strict';

exports.__esModule = true;
exports.exportToInternalScheme = exports.fetchFields = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _index = require('../../internal/index');

var _utils = require('../../utils');

var domNodeFormatter = function domNodeFormatter(node) {
  return (0, _utils.textTrimmer)(node && node.textContent || '');
};
var field = function field(name, valueRetriever, formatter, exporter) {
  return {
    name: name,
    valueRetriever: valueRetriever,
    formatter: formatter,
    exporter: exporter
  };
};

var fields = [field('name', function () {
  return document.querySelector('#profile-wrapper .pv-top-card-section__name');
}, domNodeFormatter, _utils.exportName), field('email', function () {
  return document.querySelector('.ci-email .pv-contact-info__contact-link');
}, domNodeFormatter, function (v) {
  var _ref;

  return _ref = {}, _ref[_index.constants.EMAIL] = v, _ref;
}), field('jobTitle', function () {
  return document.querySelector('#profile-wrapper .pv-top-card-section__headline');
}, domNodeFormatter, function (v) {
  var _ref2;

  return _ref2 = {}, _ref2[_index.constants.JOB_TITLE] = v, _ref2;
}), field('company', function () {
  return document.querySelector('#profile-wrapper .pv-top-card-section__company');
}, domNodeFormatter, function (v) {
  var _ref3;

  return _ref3 = {}, _ref3[_index.constants.COMPANY] = v, _ref3;
}), field('url', function () {
  return document.location.href;
}, _utils.textTrimmer, function (v) {
  var _ref4;

  return _ref4 = {}, _ref4[_index.constants.LINKEDIN_PROFILE_URL] = v, _ref4;
})];

var fetchFields = exports.fetchFields = function fetchFields() {
  return fields.reduce(function (acc, _ref5) {
    var name = _ref5.name,
        valueRetriever = _ref5.valueRetriever,
        formatter = _ref5.formatter;

    acc[name] = formatter(valueRetriever());
    return acc;
  }, {});
};

var exportToInternalScheme = exports.exportToInternalScheme = function exportToInternalScheme(socialScheme) {
  return fields.reduce(function (acc, _ref6) {
    var name = _ref6.name,
        exporter = _ref6.exporter;

    var value = socialScheme[name];
    return _extends({}, acc, exporter(value, socialScheme));
  }, {});
};