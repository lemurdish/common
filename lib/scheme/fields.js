'use strict';

exports.__esModule = true;
var text = exports.text = function text(name, exporter, importer, options) {
  return {
    type: 'text',
    name: name,
    exporter: exporter,
    importer: importer,
    options: options
  };
};

var select = exports.select = function select(name, exporter, importer, options) {
  return {
    type: 'select',
    name: name,
    exporter: exporter,
    importer: importer,
    options: options
  };
};

var foreignKey = exports.foreignKey = function foreignKey(name, exporter, importer, options) {
  return {
    type: 'foreignKey',
    name: name,
    exporter: exporter,
    importer: importer,
    options: options
  };
};