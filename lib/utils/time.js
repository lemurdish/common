'use strict';

exports.__esModule = true;
exports.DAY = exports.HOUR = exports.MINUTE = exports.SECOND = exports.getNow = undefined;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getNow = exports.getNow = function getNow() {
  return +(0, _moment2.default)();
};

var SECOND = exports.SECOND = 1000;
var MINUTE = exports.MINUTE = 60 * SECOND;
var HOUR = exports.HOUR = 60 * MINUTE;
var DAY = exports.DAY = 24 * HOUR;