'use strict';

exports.__esModule = true;
var colors = exports.colors = {
  'primary-light': '#ffe1ba',
  'primary': '#ebaf62',
  'info': '#457596',
  'success': '#44a277',
  'danger': '#cb633c'
};