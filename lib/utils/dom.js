'use strict';

exports.__esModule = true;
exports.isLemurDishDomNodeIdInserted = exports.ensureLemurDishDomNodeIdInserted = undefined;

var _constants = require('../constants');

var constants = _interopRequireWildcard(_constants);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var ensureLemurDishDomNodeIdInserted = exports.ensureLemurDishDomNodeIdInserted = function ensureLemurDishDomNodeIdInserted() {
  if (isLemurDishDomNodeIdInserted()) return;
  var node = document.createElement('div');
  node.id = constants.EXTENSION_DOM_NODE_ID;
  document.body.appendChild(node);
};

var isLemurDishDomNodeIdInserted = exports.isLemurDishDomNodeIdInserted = function isLemurDishDomNodeIdInserted() {
  return Array.from(document.body.children).some(function (node) {
    return node.id === constants.EXTENSION_DOM_NODE_ID;
  });
};