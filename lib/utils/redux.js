'use strict';

exports.__esModule = true;
exports.transformSelectorsForPath = exports.genActions = exports.genConstBase = undefined;

var _reduxActions = require('redux-actions');

var id = function id(arg) {
  return arg;
};

var genConstBase = exports.genConstBase = function genConstBase(prefix) {
  return {
    INTENT: prefix + '_INTENT',
    REQUEST: prefix + '_REQUEST',
    RESULT: prefix + '_RESULT'
  };
};

var genActions = exports.genActions = function genActions(constBase) {
  var requestCreator = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : id;
  var resultCreator = arguments[2];
  return {
    intent: (0, _reduxActions.createAction)(constBase.INTENT, requestCreator),
    request: (0, _reduxActions.createAction)(constBase.REQUEST, requestCreator),
    result: (0, _reduxActions.createAction)(constBase.RESULT, resultCreator)
  };
};

var transformSelectorsForPath = exports.transformSelectorsForPath = function transformSelectorsForPath(path, selectors) {
  var result = {};

  var _loop = function _loop(selectorName) {
    result[selectorName] = function (state) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      return selectors[selectorName].apply(selectors, [state[path]].concat(args));
    };
    Object.defineProperty(result[selectorName], 'name', {
      value: selectors[selectorName].name,
      enumerable: false,
      configurable: false,
      writable: false
    });
  };

  for (var selectorName in selectors) {
    _loop(selectorName);
  }
  return result;
};