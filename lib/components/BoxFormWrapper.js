'use strict';

exports.__esModule = true;

var _templateObject = _taggedTemplateLiteralLoose(['\n  width: ', 'px;\n  max-width: ', 'px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n'], ['\n  width: ', 'px;\n  max-width: ', 'px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n']);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _styledComponents = require('styled-components');

var _styledComponents2 = _interopRequireDefault(_styledComponents);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteralLoose(strings, raw) { strings.raw = raw; return strings; }

var Box = _styledComponents2.default.div.attrs({
  className: 'box'
})(_templateObject, function (props) {
  return props.width;
}, function (props) {
  return props.width;
});

exports.default = function (_ref) {
  var _ref$width = _ref.width,
      width = _ref$width === undefined ? 400 : _ref$width,
      children = _ref.children;
  return _react2.default.createElement(
    Box,
    { width: width },
    children
  );
};

module.exports = exports['default'];