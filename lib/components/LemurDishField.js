'use strict';

exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LemurDishField = function LemurDishField(_ref) {
  var label = _ref.label,
      type = _ref.type,
      input = _ref.input,
      successOnTouched = _ref.successOnTouched,
      _ref$meta = _ref.meta,
      touched = _ref$meta.touched,
      error = _ref$meta.error,
      warning = _ref$meta.warning;
  return _react2.default.createElement(
    'div',
    { className: 'field' },
    _react2.default.createElement(
      'label',
      {
        className: 'label',
        htmlFor: 'redux-form-field-' + input.name
      },
      label
    ),
    _react2.default.createElement(
      'div',
      { className: 'control' },
      _react2.default.createElement('input', _extends({}, input, {
        className: (0, _classnames2.default)('input', {
          'is-success': successOnTouched && touched && !warning && !error,
          'is-warning': touched && warning && !error,
          'is-danger': touched && error
        }),
        placeholder: label,
        type: type,
        id: 'redux-form-field-' + input.name,
        autoComplete: 'off',
        autoCorrect: 'off',
        autoCapitalize: 'off',
        spellCheck: 'false'
      })),
      touched ? error && _react2.default.createElement(
        'small',
        { className: 'help is-danger' },
        error
      ) || warning && _react2.default.createElement(
        'small',
        { className: 'help is-warning' },
        warning
      ) : null
    )
  );
};

LemurDishField.defaultProps = {
  type: 'text'
};

LemurDishField.propTypes = {
  label: _propTypes2.default.string.isRequired,
  type: _propTypes2.default.string
};

exports.default = LemurDishField;
module.exports = exports['default'];