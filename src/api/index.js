import 'babel-polyfill';
import URI from 'urijs';
import axios from 'axios';

export default function API(base='https://localhost:5000', axios=axios) {
  const getPath = path => {
    const uri = new URI(base);
    uri.path(path);
    return uri.toString();
  };

  async function get(path, idToken) {
    const headers = {
      'Authorization': `Bearer ${idToken}`
    };
    const response = await axios.get(getPath(path), { headers });
    return response.data;
  }

  async function post(path, msg, idToken) {
    const headers = {
      'Authorization': `Bearer ${idToken}`,
      'Content-Type': 'application/json'
    };
    const response = await axios.post(getPath(path), msg, { headers });
    return response.data;
  }

  async function fetchUser(idToken) {
    return get('/api/auth/get-user', idToken);
  }

  async function upsertUser(idToken) {
    return get('/api/auth/upsert-user', idToken);
  }

  async function saveContact(crmModel, idToken) {
    return post('/api/integrations/save-contact', crmModel, idToken);
  }

  async function fetchContact(crmModel, idToken) {
    const uri = new URI(base);
    uri.path('/api/integrations/fetch-contact');
    uri.setSearch(crmModel);
    const headers = {
      'Authorization': `Bearer ${idToken}`
    };
    const response = await axios.get(uri.toString(), { headers });
    return response.data;
  }

  async function fetchSchema(idToken) {
    return get('/api/integrations/fetch-schema', idToken);
  }
  return {
    get,
    post,
    fetchUser,
    integration: {
      saveContact,
      fetchContact,
      fetchSchema
    }
  }
}
