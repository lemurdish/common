import { parseFullName } from 'parse-full-name';
import * as internalSchemeConstants from './internal/constants';

export const exportName = (name = '') => {
  const parsedName = parseFullName(name);
  return {
    [internalSchemeConstants.FIRST_NAME]: parsedName.first,
    [internalSchemeConstants.LAST_NAME]: parsedName.last
  };
};
export const textTrimmer = text => text.trim();

