export const isRequired = v => {
  if (!v) {
    return 'This field is required';
  }
  return undefined;
};
