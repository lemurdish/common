import _ from 'lodash';
import * as constants from './constants';
import * as allValidators from './validators';

const labels = {
  [constants.FIRST_NAME]: 'First Name',
  [constants.LAST_NAME]: 'Last Name',
  [constants.EMAIL]: 'E-mail',
  [constants.JOB_TITLE]: 'Job Title',
  [constants.COMPANY]: 'Company',
  [constants.LINKEDIN_PROFILE_URL]: 'LinkedIn Profile URL',
};

const validators = {
  [constants.FIRST_NAME]: allValidators.isRequired,
  [constants.LAST_NAME]: allValidators.isRequired,
  [constants.EMAIL]: _.noop,
  [constants.JOB_TITLE]: _.noop,
  [constants.COMPANY]: _.noop,
  [constants.LINKEDIN_PROFILE_URL]: _.noop
};

export {
  constants,
  labels,
  validators
};
