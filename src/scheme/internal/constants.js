const prefix = 'INTERNAL_SCHEME';

export const FIRST_NAME           = `${prefix}_FIRST_NAME`;
export const LAST_NAME            = `${prefix}_LAST_NAME`;
export const EMAIL                = `${prefix}_EMAIL`;
export const JOB_TITLE            = `${prefix}_JOB_TITLE`;
export const COMPANY              = `${prefix}_COMPANY`;
export const LINKEDIN_PROFILE_URL = `${prefix}_LINKEDIN_PROFILE_URL`;

export const ID                   = `${prefix}_ID`;       // ID for contacts retrieved from CRM
export const VIEW_URL             = `${prefix}_VIEW_URL`; // To view contact in particular CRM