import { expect } from 'chai';
import * as validators from './validators';

describe('scheme.internal.validators', () => {
  it('isRequired correctly validates values', () => {
    expect(validators.isRequired()).to.eql('This field is required');
    expect(validators.isRequired('kozik')).to.eql(undefined);
  });
});
