export const text = (name, exporter, importer, options) => ({
  type: 'text',
  name,
  exporter,
  importer,
  options
});

export const select = (name, exporter, importer, options) => ({
  type: 'select',
  name,
  exporter,
  importer,
  options
});

export const foreignKey = (name, exporter, importer, options) => ({
  type: 'foreignKey',
  name,
  exporter,
  importer,
  options
});
