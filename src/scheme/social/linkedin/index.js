import { constants as internalSchemeConstants } from '../../internal/index';

import { exportName, textTrimmer } from '../../utils';

const domNodeFormatter = node => textTrimmer((node && node.textContent) || '');
const field = (name, valueRetriever, formatter, exporter) => ({
  name,
  valueRetriever,
  formatter,
  exporter
});

const fields = [
  field(
    'name',
    () => document.querySelector('#profile-wrapper .pv-top-card-section__name'),
    domNodeFormatter,
    exportName
  ),

  field(
    'email',
    () => document.querySelector('.ci-email .pv-contact-info__contact-link'),
    domNodeFormatter,
    v => ({ [internalSchemeConstants.EMAIL]: v })
  ),

  field(
    'jobTitle',
    () => document.querySelector('#profile-wrapper .pv-top-card-section__headline'),
    domNodeFormatter,
    v => ({ [internalSchemeConstants.JOB_TITLE]: v })
  ),

  field(
    'company',
    () => document.querySelector('#profile-wrapper .pv-top-card-section__company'),
    domNodeFormatter,
    v => ({ [internalSchemeConstants.COMPANY]: v })
  ),

  field(
    'url',
    () => document.location.href,
    textTrimmer,
    v => ({ [internalSchemeConstants.LINKEDIN_PROFILE_URL]: v })
  )
];

export const fetchFields = () =>
  fields.reduce((acc, { name, valueRetriever, formatter }) => {
    acc[name] = formatter(valueRetriever());
    return acc;
  }, {});

export const exportToInternalScheme = socialScheme =>
  fields.reduce((acc, { name, exporter }) => {
    const value = socialScheme[name];
    return {
      ...acc,
      ...exporter(value, socialScheme) // pass through the current field's value
                                       // as well as all values as a second parameter
    };
  }, {});
