import { expect } from 'chai';
import {
  fetchFields,
  exportToInternalScheme
} from './index';
import { constants as internalSchemeConstants } from '../../internal';

describe('scheme.social.linkedin', () => {
  const addNode = (parent, className, content) => {
    const node = document.createElement('div');
    node.classList.add(className);
    node.innerHTML = content;
    parent.appendChild(node);
  };

  beforeEach(() => {
    document.body.innerHTML = '';

    const profileWrapper = document.createElement('div');
    profileWrapper.id = 'profile-wrapper';
    document.body.appendChild(profileWrapper);

    // first and last name
    addNode(profileWrapper, 'pv-top-card-section__name', 'John Maria Kowalski-Nowak');

    // email
    const emailParent = document.createElement('div');
    emailParent.classList.add('ci-email');
    document.body.appendChild(emailParent);
    addNode(emailParent, 'pv-contact-info__contact-link', 'dev@lemurdish.com');

    // job title
    addNode(profileWrapper, 'pv-top-card-section__headline', 'hacker coder');

    // company
    addNode(profileWrapper, 'pv-top-card-section__company', 'LemurDish Inc.')
  });

  it('fetches all fields and exports them into internal scheme', () => {
    const socialScheme = fetchFields();
    expect(exportToInternalScheme(socialScheme)).to.eql({
      [internalSchemeConstants.FIRST_NAME]: 'John',
      [internalSchemeConstants.LAST_NAME]: 'Kowalski-Nowak',
      [internalSchemeConstants.EMAIL]: 'dev@lemurdish.com',
      [internalSchemeConstants.JOB_TITLE]: 'hacker coder',
      [internalSchemeConstants.COMPANY]: 'LemurDish Inc.',
      [internalSchemeConstants.LINKEDIN_PROFILE_URL]: 'https://app.lemurdish.com/' // defined in setup-app.js
    });
  });

  afterEach(() => {
    document.body.innerHTML = '';
  });
});
