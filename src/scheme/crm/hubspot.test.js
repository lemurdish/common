import _ from 'lodash';
import { expect } from 'chai';
import * as hubspot from './hubspot';

describe('scheme.crm.hubspot', () => {
  const incomingHubspotModel = {
    "profile-url": "https://hubspot.com/test/123",
    //#
    "properties": {
      "company": {
        "value": "LemurDish"
      },
      "email": {
        "value": "joannachristinetunstead@yahoo.co.uk"
      },
      "firstname": {
        "value": "Teodor"
      },
      "lastname": {
        "value": "Barnes"
      },
      "linkedinbio": {
        "value": "https://www.linkedin.com/in/joanna-barnes-27560589/"
      },
      "jobtitle": {
        "value": "Business Sales Consultant at Geason Training"
      }
    }
  };
  const hubspotModel = {
    "profile-url": "https://hubspot.com/test/123",
    "properties": [
      {
        "property": "firstname",
        "value": "Teodor"
      },
      {
        "property": "lastname",
        "value": "Barnes"
      },
      {
        "property": "email",
        "value": "joannachristinetunstead@yahoo.co.uk"
      },
      {
        "property": "jobtitle",
        "value": "Business Sales Consultant at Geason Training"
      },
      {
        "property": "company",
        "value": "LemurDish"
      },
      {
        "property": "linkedinbio",
        "value": "https://www.linkedin.com/in/joanna-barnes-27560589/"
      }
    ]
  };
  const internalModel = {
    INTERNAL_SCHEME_VIEW_URL: "https://hubspot.com/test/123",
    INTERNAL_SCHEME_COMPANY: "LemurDish",
    INTERNAL_SCHEME_EMAIL: "joannachristinetunstead@yahoo.co.uk",
    INTERNAL_SCHEME_FIRST_NAME: "Teodor",
    INTERNAL_SCHEME_JOB_TITLE: "Business Sales Consultant at Geason Training",
    INTERNAL_SCHEME_LAST_NAME: "Barnes",
    INTERNAL_SCHEME_LINKEDIN_PROFILE_URL: "https://www.linkedin.com/in/joanna-barnes-27560589/"
  };
  describe('for no-id profile', () => {
    it("should perform fromInternal properly", () => {
      expect(hubspot.fromInternal(internalModel)).to.eql(hubspotModel);
    });
    it("should perform toInternal properly", () => {
      expect(hubspot.toInternal(hubspotModel)).to.eql(internalModel);
    });
    it("should work for incoming profile", () => {
      expect(hubspot.toInternal(incomingHubspotModel)).to.eql(internalModel);
    });
  })
});
