import _ from 'lodash';
import * as internalSchemeConstants from '../internal/constants';
import * as utils from '../utils';

const mapping = {
  firstname:   internalSchemeConstants.FIRST_NAME,
  lastname:    internalSchemeConstants.LAST_NAME,
  email:       internalSchemeConstants.EMAIL,
  jobtitle:    internalSchemeConstants.JOB_TITLE,
  company:     internalSchemeConstants.COMPANY,
  linkedinbio: internalSchemeConstants.LINKEDIN_PROFILE_URL
};

export const fromInternal = (internalModel) => {
  const result = {};
  const properties = [];
  const profileUrl = internalModel[internalSchemeConstants.VIEW_URL];
  if (profileUrl) result['profile-url'] = profileUrl;
  _
    .toPairs(mapping)
    .forEach(
      ([hubspotProperty, internalProperty]) => {
        if (internalModel[internalProperty]) {
          properties.push({
            property: hubspotProperty,
            value: internalModel[internalProperty]
          });
        }
      }
    );
  return {
    properties,
    ...result
  };
};

export const toInternal = (hubspotModel) => {
  const result = {};
  const profileUrl = hubspotModel['profile-url'];
  if (profileUrl) result[internalSchemeConstants.VIEW_URL] = profileUrl;
  if (Array.isArray(hubspotModel.properties)) {
    hubspotModel.properties.forEach(
      ({ property, value }) => {
        if (mapping[property]) {
          result[mapping[property]] = value;
        }
      }
    );
  }
  else {
    _.keys(mapping).forEach(
      (property) => {
        const info = hubspotModel.properties[property];
        if (info && info.value) result[mapping[property]] = info.value;
      }
    )
  }
  return result;
};
