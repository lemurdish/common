import _ from 'lodash';
import { expect } from 'chai';
import * as pipedrive from './pipedrive';

describe('scheme.crm.pipedrive', () => {
  describe('for no-id profile', () => {
    const internalModel = {
      INTERNAL_SCHEME_COMPANY: "Geason Training",
      INTERNAL_SCHEME_EMAIL: "joannachristinetunstead@yahoo.co.uk",
      INTERNAL_SCHEME_FIRST_NAME: "Joanna",
      INTERNAL_SCHEME_JOB_TITLE: "Business Sales Consultant at Geason Training",
      INTERNAL_SCHEME_LAST_NAME: "Barnes",
      INTERNAL_SCHEME_LINKEDIN_PROFILE_URL: "https://www.linkedin.com/in/joanna-barnes-27560589/"
    };

    const pipedriveModel = {
      name: "Joanna Barnes",
      email: "joannachristinetunstead@yahoo.co.uk"
    };

    it('should perform fromInternal properly', () => {
      expect(pipedrive.fromInternal(internalModel)).to.eql(pipedriveModel);
    });
    it('should perform toInternal properly', () => {
      expect(pipedrive.toInternal(pipedriveModel)).to.eql(
        _.pick(
          internalModel, [
            'INTERNAL_SCHEME_FIRST_NAME',
            'INTERNAL_SCHEME_LAST_NAME',
            'INTERNAL_SCHEME_EMAIL'
          ])
      )
    });
  });
  describe('for profile with id', () => {
    const internalModel = {
      INTERNAL_SCHEME_ID: 11,
      INTERNAL_SCHEME_VIEW_URL: "https://app.pipedrive.com/person/11",
      INTERNAL_SCHEME_COMPANY: "Geason Training",
      INTERNAL_SCHEME_EMAIL: "joannachristinetunstead@yahoo.co.uk",
      INTERNAL_SCHEME_FIRST_NAME: "Joanna",
      INTERNAL_SCHEME_JOB_TITLE: "Business Sales Consultant at Geason Training",
      INTERNAL_SCHEME_LAST_NAME: "Barnes",
      INTERNAL_SCHEME_LINKEDIN_PROFILE_URL: "https://www.linkedin.com/in/joanna-barnes-27560589/"
    };

    const pipedriveModel = {
      name: "Joanna Barnes",
      email: "joannachristinetunstead@yahoo.co.uk",
      id: 11
    };

    it('should perform fromInternal properly', () => {
      expect(pipedrive.fromInternal(internalModel)).to.eql(pipedriveModel);
    });
    it('should perform toInternal properly', () => {
      expect(pipedrive.toInternal(pipedriveModel)).to.eql(
        _.pick(
          internalModel, [
            'INTERNAL_SCHEME_FIRST_NAME',
            'INTERNAL_SCHEME_LAST_NAME',
            'INTERNAL_SCHEME_EMAIL',
            'INTERNAL_SCHEME_ID',
            'INTERNAL_SCHEME_VIEW_URL'
          ])
      )
    });
  });
});
