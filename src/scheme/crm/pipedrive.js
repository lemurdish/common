import * as fields from '../fields';
import * as internalSchemeConstants from '../internal/constants';
import * as utils from '../utils';

export const schema = [
  fields.text(
    'name',
    model => utils.exportName(model.name),
    internalModel => {
      const first = internalModel[internalSchemeConstants.FIRST_NAME];
      const last = internalModel[internalSchemeConstants.LAST_NAME];
      return {
        name: `${first} ${last}`
      };
    }
  ),
  fields.text(
    'email',
    model => model.email
      ? ({ [internalSchemeConstants.EMAIL]: model.email })
      : ({}),
    internalModel => {
      const email = internalModel[internalSchemeConstants.EMAIL];
      if (email) return ({ email });
      return {};
    }
  ),
  fields.text(
    'url',
    model => model.id
      ? ({ [internalSchemeConstants.VIEW_URL]: `https://app.pipedrive.com/person/${model.id}` })
      : ({}),
    internalModel => ({})
  ),
  fields.text(
    'id',
    model => model.id
      ? ({ [internalSchemeConstants.ID]: model.id })
      : ({}),
    internalModel => {
      const id = internalModel[internalSchemeConstants.ID];
      if (id) return { id };
      return null;
    }
  )
];

export const toInternal = (crmModel) => schema.reduce(
  (acc, field) => ({
    ...acc,
    ...field.exporter(crmModel)
  }), {}
);

export const fromInternal = (internalModel) => schema.reduce(
  (acc, field) => ({
    ...acc,
    ...field.importer(internalModel)
  }), {}
);

