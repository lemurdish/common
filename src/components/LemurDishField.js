import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const LemurDishField = ({
  label,
  type,
  /* eslint-disable react/prop-types */
  input,
  successOnTouched,
  meta: { touched, error, warning }
  /* eslint-enable react/prop-types */
}) => (
  <div className="field">
    <label
      className="label"
      htmlFor={`redux-form-field-${input.name}`}
    >{label}</label>
    <div className="control">
      <input
        {...input}
        className={
          classnames(
            'input', {
              'is-success': successOnTouched && touched && !warning && !error,
              'is-warning': touched && warning && !error,
              'is-danger': touched && error
            }
          )
        }
        placeholder={label}
        type={type}
        id={`redux-form-field-${input.name}`}
        autoComplete="off"
        autoCorrect="off"
        autoCapitalize="off"
        spellCheck="false"
      />{
        touched
          ? (
            (error && <small className="help is-danger">{error}</small>) ||
            (warning && <small className="help is-warning">{warning}</small>)
          ) : null
    }</div>
  </div>
);

LemurDishField.defaultProps = {
  type: 'text'
};

LemurDishField.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string
};

export default LemurDishField;
