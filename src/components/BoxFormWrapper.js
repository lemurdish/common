import React from 'react';
import styled from 'styled-components';

const Box = styled.div.attrs({
  className: 'box'
})`
  width: ${props => props.width}px;
  max-width: ${props => props.width}px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default ({ width=400, children }) => (
  <Box width={width}>
    {children}
  </Box>
)