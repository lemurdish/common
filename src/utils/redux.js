import { createAction } from 'redux-actions';
const id = arg => arg;

export const genConstBase = (prefix) => ({
  INTENT: prefix + '_INTENT',
  REQUEST: prefix + '_REQUEST',
  RESULT: prefix + '_RESULT'
});

export const genActions = (constBase, requestCreator=id, resultCreator) => ({
  intent: createAction(constBase.INTENT, requestCreator),
  request: createAction(constBase.REQUEST, requestCreator),
  result: createAction(constBase.RESULT, resultCreator)
});

export const transformSelectorsForPath = (path, selectors) => {
  const result = {};
  for (let selectorName in selectors) {
    result[selectorName] = (state, ...args) => selectors[selectorName](state[path], ...args);
    Object.defineProperty(
      result[selectorName],
      'name', {
        value: selectors[selectorName].name,
        enumerable: false,
        configurable: false,
        writable: false
      });
  }
  return result;
};
