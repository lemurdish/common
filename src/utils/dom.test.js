import { expect } from 'chai';
import {
  ensureLemurDishDomNodeIdInserted,
  isLemurDishDomNodeIdInserted
} from './dom';

describe('utils.dom', () => {
  beforeEach(() => {
    document.body.innerHTML = '';
  });

  it('injects a fake DOM element and checks if it exists', () => {
    expect(isLemurDishDomNodeIdInserted()).to.eql(false);
    ensureLemurDishDomNodeIdInserted();
    expect(isLemurDishDomNodeIdInserted()).to.eql(true);
  });

  afterEach(() => {
    document.body.innerHTML = '';
  });
});
