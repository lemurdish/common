import * as constants from '../constants';

export const ensureLemurDishDomNodeIdInserted = () => {
  if (isLemurDishDomNodeIdInserted()) return;
  const node = document.createElement('div');
  node.id = constants.EXTENSION_DOM_NODE_ID;
  document.body.appendChild(node);
};

export const isLemurDishDomNodeIdInserted = () => {
  return Array.from(document.body.children).some(node => node.id === constants.EXTENSION_DOM_NODE_ID);
};
