const shell = require('shelljs');
const fs = require('fs');
const chalk = require('chalk');

const inDir = (dir) => f => {
  const cur = shell.pwd();
  try {
    shell.mkdir('-p', dir);
    shell.cd(dir);
    f();
  }
  finally {
    shell.cd(cur);
  }
};

const EXTENSIONS = [
  {
    url: 'https://raw.githubusercontent.com/Wikiki/bulma-steps/master/steps.sass',
    name: 'steps.sass'
  }, {
    url: 'https://raw.githubusercontent.com/Wikiki/bulma-pageloader/master/pageloader.sass',
    name: 'pageloader.sass'
  }
];

logSuccess = (...args) => console.log(chalk.green('\u2714'), ...args);

function createFile(fileName, content) {
  fs.writeFileSync(fileName, content);
  logSuccess('Created file', fileName);
}

function cloneBulma() {
  inDir('./third_party')(() => {
    shell.exec('rm -r bulma');
    shell.exec('git clone git@github.com:jgthms/bulma.git');
    logSuccess('Cloned bulma');
  });
}

function getAllContent() {
  const lines = ['@charset "utf-8"\n'].concat(EXTENSIONS.map(ext => `@import "${ext.name}"`));
  return lines.join('\n');
}

function installExtensions() {
  inDir('./third_party/bulma/sass/extensions')(() => {
    EXTENSIONS.forEach((ext) => {
      shell.exec(`curl ${ext.url} > ${ext.name}`);
      logSuccess('Installed', ext.name);
    });
    createFile('_all.sass', getAllContent());
  });
}

function importExtensions() {
  inDir('./third_party/bulma')(() => {
    shell.exec(`echo '@import "sass/extensions/_all"' >> ./bulma.sass`);
  });
}

function customizeBulma() {
  const orig = fs.readFileSync('./third_party/bulma/bulma.sass', 'utf8');
  const custom = fs.readFileSync('./bulma_customization.sass', 'utf8');
  fs.writeFileSync('./third_party/bulma/bulma.sass', custom + '\n' + orig);
  logSuccess('Customized bulma');
}

function buildBulma() {
  inDir('./third_party/bulma')(() => {
    shell.exec('npm install');
    logSuccess('Installed bulma dependencies');
    shell.exec('npm run build');
    logSuccess('Built bulma');
  });
}

function cleanBulma() {
  inDir('./third_party/')(() => {
    shell.cp('bulma/css/bulma.css', 'bulma.css');
    shell.exec('rm -r bulma/');
    logSuccess('Cleaned up');
  });
}

function setUpBulma() {
  cloneBulma();
  installExtensions();
  importExtensions();
  customizeBulma();
  buildBulma();
  cleanBulma();
}

setUpBulma();

